"""
Python Module: contains functions needed to conduct processes on input aruguments
- in assistance to app3_bpsk.py
"""

from warnings import simplefilter
simplefilter("ignore")

import subprocess
import numpy as np
from xminter.xmidas import xm
import bluefile
from deframing import remove
from scipy.signal  import firwin, lfilter

'''
Image Restoration
- data coming from deframing
'''
def convert(input_arr, name): #TO-DO: getoutput output in terminal changes depending on run (not always correct)
    input_arr.tofile(name)
    cmd = "cpio -ivF {:s}".format(name)
    out_fname = subprocess.getoutput(cmd)
    print(out_fname)
    out_fname = out_fname.split()[1]

    return out_fname

'''
Deframing
- data coming from demodulator
'''
def deframe(input_file, word):
    cpio_arr = np.fromfile(input_file, dtype = np.uint8)
    deframe_arr = remove(cpio_arr, word)

    return deframe_arr
    
'''
Demodulator
- data coming from equalizer
'''
def demod(input_file): # switched to BPSK
    xm.reformat(input_file, "input_floats", "CF")
    _, get_data = bluefile.read("input_floats", {})

    symbs = []

    p = np.arctan2(get_data.imag, get_data.real)

    for i in range(len(p)):
        selection = p[10*i:(10*i)+10]
        added = sum(selection)
        if (added < 0):
            symbs.append(0)
        if (added > 0):
            symbs.append(1)

    packed_symbs = np.packbits(np.array(symbs))

    return packed_symbs

'''
Leveling
- Create QPSK Constellation Points and Leveled Phase Plot
'''
def despin(input_file, output_file): # this is currently for the phase locked version transmission
    
    xm.reformat(input_file, "input_floats", "CF")
    hdr, x = bluefile.read("input_floats", {})

    # needed for real transmission below is for no transmission
    #p = np.arctan2(x.imag, x.real)
    #pl = np.mean(p) 

    # testing
    pl = np.pi/2

    x *= np.cos(pl) - 1j*np.sin(pl)

    bluefile.write(output_file, hdr, x)
   
'''
Turn Blue File into Array + First Set of Filtering
- Obtain Header Information
'''
def reformat(input_file, output_file): # use if sbtuner is not used, this substitutes as the filter part of sbtune
    xm.reformat(input_file, "input_floats", "CF")
    hdr, x = bluefile.read("input_floats", {})
    x = lfilter(firwin(255, 0.04), 1, x)
    bluefile.write(output_file, hdr, x)

    return hdr

'''
Filtering Raw Data
'''
def filter(input_file, output_file, tstart, tend, lo_freq, hi_freq):

    # sbtuner
    """
    input_if = (lo_freq + hi_freq) / 2.0
    bandwidth = hi_freq - lo_freq
    xm.sbtuner("thinned_file", output_file, input_if, bandwidth, 0.0, 1, "CF", exact=True) 
    
    """
    xm.thin(input_file, output_file, tstart, tend, None, "ABSCISSA")
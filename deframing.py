import numpy as np

'''
Search and Remove
'''
def remove(data_list, word):
    ascii_pos = find_ascii(data_list, word)
    center = data_list[ascii_pos[0]+14:ascii_pos[1]-8] # 6 byte word and (2) 8 byte size
    counter_pos = counter(center, data_list, word)

    removed = np.delete(center, counter_pos)

    total_size = size_real(data_list, word)
    subtract = len(removed) - total_size
    without_end = removed[0: len(removed)-subtract]
    
    return without_end

'''
0's Frame
- Front: 1010 bytes
- Back: 1010 bytes
'''
def find_zeros(data_list, word): # returns start and end of frame to look at
    ascii_pos = find_ascii(data_list, word)
    end_front = ascii_pos[0]
    start_back = ascii_pos[1]
    filler = 1010
    start = end_front-filler
    end = start_back+filler+6

    return start, end

'''
ascii 
- 6 bytes
'''
def ascii_rep(word): # 6 ubyte array
    ascii_array = [] 
    char_list = []

    char_list = [(word[i:i+1]) for i in range(0, len(word), 1)]
    ascii_array = [(ord(i)) for i in char_list]

    return ascii_array

def find_ascii(data_list, word): # find the pattern in the whole input data
    ascii_pattern = ascii_rep(word)
    index_ascii_start = []

    N = len(data_list)
    M = len(word)

    for i in range(N - M + 1):
        j = 0
         
        while(j < M):
            if (data_list[i + j] != ascii_pattern[j]):
                break
            j += 1
 
        if (j == M):
            index_ascii_start.append(i)

    return index_ascii_start

'''
size
- 8 bytes (1 decimal representation - 64 bits)
'''
def size_center(data_list, word): # including counters - not used
    ascii_pos = find_ascii(data_list, word)
    start = ascii_pos[0] + 6
    end = start + 8 

    size = data_list[start:end]

    bin_array = [format(i,'08b') for i in size]
    string_bin = ''.join(bin_array)
    data_size = int(string_bin,2)

    return data_size

def size_real(data_list, word):
    ascii_pos = find_ascii(data_list, word)
    start = ascii_pos[1] - 8
    end = ascii_pos[1]

    size = data_list[start:end]

    bin_array = [format(i,'08b') for i in size]
    string_bin = ''.join(bin_array)
    data_size = int(string_bin,2)

    return data_size

'''
counters
- 1 byte each
'''
def counter(center, data_list, word):
    counter_pos = []
    count = []
    size = size_center(data_list, word)
    num_counters = size/1024

    j = 0
    k = 1
    for i in range(0, int(num_counters)):
        if (j > 255):
            k += 1
            j = 0
        if (i>255*k):
            count.append(j)
            j += 1
        else:
            count.append(i)

    i = 1023
    j = 0
    while i < len(center):
        if (center[i] == count[j]):
            counter_pos.append(i)
            j += 1

        i += 1024

    return counter_pos
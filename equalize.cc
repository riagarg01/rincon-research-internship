// C++ Pirimitve Writing Template Importing X-MIDAS

#include <primitive.h>
#include <vector>
#include <complex>
#include <typeinfo>

void mainroutine() {
    CPHEADER hin;
    m_init(hin, m_apick(1), "1000", "CI", 0);
    m_inok(hin);

    CPHEADER hout;
    m_propagate(hin, hout);
    hout.file_name = m_apick(2);
    hout.format = "SF"; // if need to change format - otherwise would have been same as the init
    m_allocate(hout);

    int32_t ngot = 0;
    int32_t tl = m_get_lswitch_def("TL", 1024);
    hin.xfer_len = hin.cons_len = tl;
    vector< complex<float> > input(tl, 0.0);
    vector< float > output(tl);
    
    m_sync();

   
    while(!Mc->break_) {
        m_grabx(hin, &input[0], ngot);
        if(ngot <= 0) {
            break;
        }                   

        m_filad(hout, &output[0], ngot);
    }

    m_close(hin);
    m_close(hout);
}

/*
    for(int i=0; i < input.size(); i++) {
        float a = real(input[i]);
        float b = imag(input[i]);
        float rad = atan(b/a);
        float rr = sqrt(pow(a,2) + pow(b,2));
        int theta = (rad * 180)/M_PI;
        output[i] = theta;
    }
*/

    
/*
	for (int i=0; i < input.size(); i++){
            for (int j=0; j < 1; j++){
                vector<float> val;
                points.push_back(val);
                points[i].push_back(i);
            }
                        
        }
*/

/*
        // getting angle values
        for(int i=0; i < input.size(); i++) {
            float a = real(input[i]);
            float b = imag(input[i]);
            float rad = atan(b/a);
            int theta = (rad * 180)/M_PI;
        }

        // rotating by 45 degrees
        int count = 0;
        int n = 2;
        int m = points.size();

        while (count < 2*n-1) {
            for (int i=0; i < m; i++) {
                for (int j=0; j < n; j++) {
                    if (i+j == count) {
                        rotated.push_back(points[i][j]);
                    }
                }
            }
        }

        cout << rotated << endl;        
*/

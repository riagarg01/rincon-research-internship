    #TO-DO: mimicking PLL

    fs = 1.0 / hdr["xdelta"]
    ts = hdr["xdelta"] * hdr["size"]
    l = int(round(ts*fs)) 
    t = np.linspace(0, ts-1/fs, l)
    y = []

    chunk = 10
    xd = int(len(x)/chunk)
    for i in range(chunk):
        xs = x[i*xd: (i+1)*xd]
        ts = len(xs)/fs
        t = np.linspace(0, ts-1/fs, xd) # time axis
        if (i == 0):
            pl = 10
            while (pl > 1 or pl < -1):
                p = np.arctan2(xs.imag, xs.real)
                pl = np.mean(p)
                d = lfilter(firwin(255, 0.04), 1, p)
                d = np.diff(p)    
                d = lfilter(firwin(255, 0.01), 1, d[np.where(abs(d)<0.01)])
                f = (np.mean(d) * fs / (2*np.pi))
                xs *= np.cos(2*np.pi*f*t + pl) - 1j*np.sin(2*np.pi*f*t + pl)
                print("0: ", f)
            y = np.concatenate((y,xs)) 
        else:
            pl = 10
            while (pl > 1 or pl < -1):
                pl = p[-1]
                p = np.arctan2(xs.imag, xs.real)
                d = lfilter(firwin(255, 0.04), 1, p)
                d = np.diff(p)    
                d = lfilter(firwin(255, 0.01), 1, d[np.where(abs(d)<0.01)])
                f = (np.mean(d) * fs / (2*np.pi))
                xs *= np.cos(2*np.pi*f*t + pl) - 1j*np.sin(2*np.pi*f*t + pl)
                print("{}: {}".format(i, f))
            y = np.concatenate((y,xs))

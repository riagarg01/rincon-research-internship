# Imports and Call-In's
import sys
import subprocess
import bluefile

from PyQt5.QtCore import QDir, Qt
import PyQt5.QtCore
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QApplication, QFileSystemModel, QWidget
from PyQt5.QtCore import QTimer
from XMinter import xm, get_msg, send_msgl, msg_peek
from xmframe import QtXPrimitive

from bpsk import Ui_application_3
from run_process import filter, reformat, despin, demod, convert, deframe

"""
Main Class
- runs GUI and all functions from imports
"""
class app_3(QWidget, Ui_application_3):

    # Constructor and Initialization
    def __init__(self, file_directory, app_path, ascii_word, parent=None):
        super(app_3, self).__init__(parent)

        # initalizations
        self.timer = QTimer(self)
        self.last_zoom = None
        self.path = file_directory
        self.stores_images = app_path
        self.model = QFileSystemModel()
        self.input_file = ''
        self.sbtuner_file = 'sbtune.tmp'
        self.pass_to_filter = 'despin_on.tmp'
        self.filtered_file = 'filtered.tmp'
        self.F = 0
        self.demod_arr = []
        self.word = ascii_word
        self.deframe_arr = []
        self.rastor_deframe = "deframe_rastor.tmp"
        self.name = ''
        self.image_path = ''
        self.hdr = dict()

        # run widgets
        self.setup_ui()
        
    # Make Connections
    def setup_ui(self):
        self.setupUi(self)

        # connections (buttons)
        self.subband_tune.clicked.connect(self.sbtune)
        self.de_spin.clicked.connect(self.equalize)
        self.synchronize.clicked.connect(self.capture_sorted)
        self.compile.clicked.connect(self.sort)
        self.load_image.clicked.connect(self.show_image)
        self.print.clicked.connect(self.print_image)

        # tree-view
        self.model.setRootPath((QDir.rootPath()))
        self.file_explorer.setModel(self.model)
        self.file_explorer.setRootIndex(self.model.index(self.path))
        self.file_explorer.setSortingEnabled(True)
        self.file_explorer.doubleClicked.connect(self.get_file) # signal connection from treeview
        
        # start plotters
        xm.message("up")
        xm.msgmask("ADD", 0, "zoom_mask", "ZOOM")
        QtXPrimitive(self.original_rastor, xm.xrast, None, None, None, "lo", all=True, stay=True, id=12, mask="zoom_mask", msgid=5)
        QtXPrimitive(self.phase, xm.xplot, None, None, None, "phase", 
                    trace="(LINE=None,SYM=Pixels)", stay=True, all=True, id=10)
        QtXPrimitive(self.iq, xm.xplot, None, None, None, "IR", None, None, -500, 500, 
                     trace="(LINE=None,SYM=Pixels)", stay=True, all=True, id=11,
                     noreadout=True, grid=False)
        QtXPrimitive(self.pre_deframing, xm.xraster, None, None, None, None, 8192, stay=True, autorr=True, id=13)
        QtXPrimitive(self.after_deframing, xm.xraster, None, None, None, None, 8192, stay=True, autorr=True, id=14)

        # timer
        self.timer.timeout.connect(self.check_for_messages)
        self.timer.setInterval(100)
        self.timer.setSingleShot(True)
        self.timer.start()
    
    # Every 10 sec check Message Queue for Updates in GUI
    def check_for_messages(self):
        num_waiting = msg_peek(5)

        if num_waiting > 0:
            while(num_waiting > 0):
                mqd = get_msg(5)[1]
                num_waiting -= 1
            self.last_zoom = mqd

        self.timer.start()

    # Load and Store User Selected File
    def get_file(self, signal):
        self.input_file = self.model.filePath(signal)
        xm.ubiq(self.input_file, "ubiq", "32k", "BH92", 0.5, "AC", 20)
        send_msgl('NEWLFN', 12, filename="ubiq")

    # Use sbtuner on Raw Data - store zoomed selection as data to be developed
    def sbtune(self):
        if self.last_zoom == None:
            return

        self.hdr = reformat(self.input_file, self.pass_to_filter) # doing a filter (no sbtune - but plotting where it would be)

        tstart = self.last_zoom["ymin"]
        tend = self.last_zoom["ymax"]
        lofreq = self.last_zoom["xmin"]
        hifreq = self.last_zoom["xmax"]

        filter(self.pass_to_filter, self.sbtuner_file, tstart, tend, lofreq, hifreq) # thinning - removed sbtune

        send_msgl('NEWLFN', 10, filename=self.sbtuner_file)
        send_msgl('NEWLFN', 11, filename=self.sbtuner_file)

        xm.ubiq(self.sbtuner_file, "ubiq", "32k", "BH92", 0.5, "AC", 20)
        send_msgl('NEWLFN', 12, filename="ubiq")

    # Keep Flattening Data Until Desired Locations on IQ Plot (de-spin button: keeps updating frame on each button click)
    def equalize(self):
        despin(self.sbtuner_file, self.filtered_file)
        send_msgl('NEWLFN', 10, filename=self.filtered_file)
        send_msgl('NEWLFN', 11, filename=self.filtered_file)
        self.sbtuner_file = self.filtered_file

    # Take Final De-Spined Data and Store as Data to be Developed
    def capture_sorted(self): #TO-DO: write synch method
        # synchronize data to filter out tails (should show counters in raster plot)
        send_msgl('NEWLFN', 13, filename=self.filtered_file + "(fc=sp)")        

    # Run Demod, Deframer, and CPIO
    def sort(self): #TO-DO: Fix this section not working correctly as for GUI Plots
        self.demod_arr = demod(self.filtered_file)
        self.image_form = convert(self.demod_arr, "before_structure.cpio")
        self.deframe_arr = deframe(self.image_form, self.word)

        self.deframe_arr.tofile("deframe_arr.dat")
        xm.dataimport("deframe_arr.dat", "deframe_rastor.tmp", "CF")
        send_msgl('NEWLFN', 14, filename="deframe_rastor.tmp")


        # adding conditions to organize files as they enter directory
        #self.first_cpio_path = '{}/{}'.format(self.stores_images, self.image_form)
        #subprocess.call("mv {} {}/store_creations/".format(self.first_cpio_path, self.stores_images), shell=True)
        
    # Show Image on Screen
    def show_image(self):
        self.name = convert(self.deframe_arr, "completion.cpio")
        self.image_path = '{}/{}'.format(self.stores_images, self.name)
    
        self.final_image.setPixmap(QPixmap(self.image_path).scaled(326, 262, Qt.KeepAspectRatio))

        #subprocess.call("mv {} {}/store_creations/".format(self.image_path, self.stores_images), shell=True)

    # Print Final Image
    def print_image(self):
        # connect to an application to print
        i=0
    
    # Stop Application When Closing GUI
    def closeEvent(self, e):
        for ii in range(10,15):
            send_msgl("EXIT", ii)

        xm.message("DOWN")
        xm.verify("OFF")

        super(app_3, self).closeEvent(e)

"""
Run Application
"""
def main():

    # locations
    file_directory = "/home/rga/raptor22/application3/blue_files"
    app_path = "/home/rga/raptor22/application3"
    ascii_word = 'raptor'

    # run application
    app = QApplication(sys.argv)
    wid = app_3(file_directory, app_path, ascii_word)
    wid.show()
    app.processEvents()
    app.exec_()

if __name__ == "__main__":
    main()
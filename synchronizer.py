import numpy as np
from xminter.xmidas import xm
import bluefile
import sys

xm.reformat(sys.argv[1], "input_floats", "CF")
hdr, x = bluefile.read("input_floats", {})

p = np.arctan2(x.imag, x.real)
pl = np.mean(p)

x *= np.cos(pl) - 1j*np.sin(pl)
p = np.arctan2(x.imag, x.real)

symbs = []

max_s = 0
count = 0
max_a = 0



for i in range(10):
    max_s = 0
    xs = x[int(0.45*len(x)):int(0.5*len(x))]
    new_p = np.roll(np.arctan2(xs.imag, xs.real), i)
    print(len(new_p))
    for j in range(len(new_p)):
        selection = new_p[10*j:(10*j)+10]
        added = sum(selection)
        if (added < 0):
            symbs.append(0)
        if (added > 0):
            symbs.append(1)
        if (added > max_s):
            max_s = np.abs(added)
        if (j%10000 == 10000-1):
            print(j, max_s, max_a, count)
            
    if (max_s > max_a):
        max_a = max_s
        count = i  

p = np.roll(p, count)

#TO-DO: added starts to print all 0's after a couple iterations
for j in range(len(p)):
    selection = p[10*j:(10*j)+10]
    added = sum(selection)
    if j%100000 == 0:
        print(added, 0.6*max_a)
    if (added < -0.6*max_a):
        symbs.append(0)
    if (added > 0.6*max_a):
        symbs.append(1)

print(len(symbs))

np.array(symbs).tofile("compare.dat")
print(symbs)

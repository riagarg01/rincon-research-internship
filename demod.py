# Primary demod written

import sys
import numpy as np
from PIL import Image as im

def demodulate(symbVec):
    binaryString = ''
    decNum = []
    
    for i in range(0, len(symbVec)):
        element = symbVec[i]
        real = element.real
        imag = element.imag
        if (real < 0) and (imag > 0):
            binaryString = binaryString + '01'
        if (real > 0) and (imag > 0):
            binaryString = binaryString + '00'
        if (real > 0) and (imag < 0):
            binaryString = binaryString + '11'
        if (real < 0) and (imag < 0):
            binaryString = binaryString + '10'

    eightBit = [binaryString[i:i+8] for i in range(0, len(binaryString), 8)]

    for i in range(0, len(eightBit)):
        val = eightBit[i]
        num = int(val,2)
        decNum.append(num)

    finalPix = (np.array(decNum).reshape(426, 640, 3)).astype(np.uint8)
    picture = im.fromarray(finalPix)
    return picture

def main():
    fileName = sys.argv[1] # taking argument from command line (.dat) and using as input to test
    symbVec = np.fromfile(fileName,dtype=np.cdouble)
    picture = demodulate(symbVec)
    picture.save('result.png')

   
if __name__ == '__main__':
    main()

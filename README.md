# Utilizing RRC’s Raptor SDR to Transmit Images over RF
This project contains the important code I wrote and was advised on during my Summer 2022 internship at RRC.

# Purpose
Create Applications that Display Digital Signal Processing (DSP) Techniques

# Objectives
- Transmit a Modulated Image over radio frequencies (RF) using a Raptor SDR
- Receive and Capture the Transmission using a Raptor software digital radios (SDR)
- Demodulate Captured Transmission and Display Transmitted Image
- Use Applications as a Recruiting Demonstration

# Issues to be Resolved
- Complete Synthesizer Function
- Align GUI Connections to Correspond with Data

# Improve Robustness of Application
- Remove Phase Locked Referenced Raptors
- Time Detector
- De-Spreading
- BSPK to QPSK

## Application 1: Transmitter

```mermaid
graph TD
    A[JPEG or BMP] -->|Data| B(Framing)
    B -->|Framed Data| C(Forward Error Correction)
    C -->|Encoded Data| D(Symbol Generation)
    D -->|Baseband Symbols| E(Spreading)
    E -->|Spread Baseband Symbols| F(Transmit)
    B -->|Framed Data| G(XRASTER)
    C -->|Encoded Data| H(XRASTER)
    D -->|Baseband pre-D| I(UBIQUITOUS)
    I -->|PSD| J(XRASTER)
    E -->|Spread Baseband Symbols| K(UBIQUITOUS)
    K -->|PSD| L(XRASTER)
```

## Application 2: Receive and Capture

```mermaid
graph TD
    A[Receive] -->|pre-D| B(CYC)
    A -->|pre-D| I(Delay Line)
    B -->|Cyclo-Spectrum| F(XRTPLOT)
    B -->|Cyclo-Spectrum| G(XRTRASTER)
    B -->|PSD| E(XRTPLOT)
    B -->|PSD| C(XRTRASTER)
    B -->|Cyclo-Spectrum| H(Threshold)
    I -->|pre-D| J(Capture)
    H -->|Trigger| J
```
## Application 3: Demodulator
```mermaid
graph TD
    A[File Detect] -->|pre-D| B(UBIQ)
    B --> |PSD|C(XRASTER)
    A[File Detect] -->|pre-D| D(FILTER)
    D -->|Baseband pre-D| E(XPLOT)
    E --> |Synchronizer| F(DE-SPIN)
    F -->|Baseband pre-D| E
    F -->|pre-D| K(UBIQ)
    K -->|PSD| L(XRASTER)
    F -->|Equalized Data| G(DEFRAMER)
    G -->|Deframed Data| H(DEMODULATOR)
    H -->|pre-D| M(UBIQ)
    M -->|PSD| P(XRASTER)
    H -->|Bytes| I(CPIO)
    I -->|pre-D| N(UBIQ)
    N -->|PSD| O(XRASTER)
    I -->|Raw Image File| J(DISPLAY/PRINT)    
```

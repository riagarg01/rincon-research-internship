from webbrowser import get
import bluefile
import numpy as np
import sys
from xminter.xmidas import xm
from deframing import remove
import subprocess

xm.reformat(sys.argv[1], "input_floats", "CF")
hdr, get_data = bluefile.read("input_floats", {})

pl = np.pi/2
symbs = []
complex_phase = []
decNum = []

get_data *= np.cos(pl) - 1j*np.sin(pl)
p = np.arctan2(get_data.imag, get_data.real)

for i in range(len(p)):
    selection = p[10*i:(10*i)+10]
    added = sum(selection)
    if (added < 0):
        symbs.append(0)
    if (added > 0):
        symbs.append(1)

packed_symbs = np.packbits(np.array(symbs))

packed_symbs.tofile("before_frame.cpio")
cmd = "cpio -ivF {:s}".format('before_frame.cpio')
out_fname = subprocess.getoutput(cmd)
out_fname = out_fname.split()[1]

cpio_arr = np.fromfile(out_fname, dtype = np.uint8)

deframe_arr = remove(cpio_arr, "raptor")

bluefile.write("/scratch/rga/putingui", hdr, deframe_arr)

deframe_arr.tofile("deframed.cpio")
cmd = "cpio -ivF {:s}".format('deframed.cpio')
out_f2name = subprocess.getoutput(cmd)
out_f2name = out_f2name.split()[1]

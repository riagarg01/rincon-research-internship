# Provided Code by Company on How to do a PLL

import numpy as np 
import matplotlib.pyplot as plt

TWOPI = 2.0*np.pi

class PLL:
    def __init__(self,fSample,bw,damping=0.707):
        self.blk        = 0 # counter of data blocks processed
        self.fSample    = fSample # sample rate in Hz
        self.dt         = 1.0/fSample # time betw each samp, sec

        # The all magical loop filter coefficients.  These coefficients dictate
        # how well our control system reacts to the input:  Stability, settling time
        # percent overshoot.  There are better ways to set these using loop-filter
        # equations and fancy-pants control theory (root-locus, etc).
        self.bw         = bw # not implemented yet
        self.damping    = damping # not implemented yet
        self.Kp         = 0.05 # proportional coeff.
        self.Ki         = 0.5*(self.Kp)**2 # integration coeff.
        self.Knco       = 1.0 # NCO scaling value (not implemented)

        self.lockVal    = 0.0 # smoothed lock value
        self.lockState  = 'IDLE' # state machine lock status
        self.lockThresh = 70.0 # percent of energy to be "locked"
        self.lockAlpha  = 0.02 # coeff for exp smoothing lock value
        self.freqAlpha  = 0.01 # coeff for exp smoothing freq estimates
        self.freqEst    = 0.0 # NCO estimate for frequency offset
        self.phaseNco   = 0.0 # keep track of NCO phase over time
        self.sumError   = 0.0 # keep track of integration error
        self.tStart     = 0.0 # start time of first sample

        return

    # set loop filter coefficients:  TODO
    def setLoopFilterCoeffs(self,bw,damping):

        return

    def process(self,signalDataIn):

        #----debugging arrays
        lockVals = []
        freqVals = []
        ncoVals  = []
        lockValsSmooth = []
        intErrors = []
        tvec = []

        #--------------------

        signalDataOut = []
        nSamps = len(signalDataIn)
        tBlkStart = self.blk * nSamps * self.dt + self.tStart

        for i in range(0,nSamps):
            #********Generate NCO Signal and Get Phase Error
            nco        = np.exp(1j*self.phaseNco)
            mixedVal   = signalDataIn[i][:]*np.conj(nco) 
            phaseError = np.angle(mixedVal)[0] # drive this to zero.

            #*********Loop Filter Using PI Controller
            # adjust our NCO phase using proportional error and integration error
            # the latter allows us to hone in on any frequency offsets in our NCO
            # integration of the error inherently has "memory"  If we are accumulating
            # phase error then we probably have a frequency offset.  The proportional error
            # helps us maintain phase alignment.  
            self.sumError  = self.Ki * phaseError + self.sumError
            self.propError = self.Kp * phaseError
            phaseAdjust    = self.sumError + self.propError
            self.phaseNco  = (self.phaseNco + phaseAdjust)%TWOPI

            #*********Update Lock Detector Value
            # We'll use percentage of energy in the in-phase branch
            # as a lock value.  If we are phase-locked correctly, most 
            # of the signal's output energy will be in the in-phase 
            # branch.
            energy  = mixedVal.imag**2 + mixedVal.real**2
            lockVal = (mixedVal.real**2)/energy*100.0 
            # Smooth the noisey lock value so we don't get false lock status
            self.lockVal = self.lockAlpha * lockVal + (1 - self.lockAlpha)*self.lockVal
            if self.lockVal > self.lockThresh:
                self.lockState = 'LOCK'
            else:
                self.lockState = 'UNLOCKED'

            #***********Estimate the frequency offset by smoothing Integration error
            # our integration error is essentially a measurement of a frequency offset
            # We can use this to infer the frequency of the incoming signal.  This estimate
            # is pretty noisey, so let's smooth it.
            currFreqEst  = self.sumError/(2*np.pi*self.dt) # convert radians/sample to Hz
            self.freqEst = self.freqAlpha * currFreqEst + (1-self.freqAlpha) * self.freqEst

            #**********Write out the baseband data
            signalDataOut.append(mixedVal)

            #**********Save data for debug plots
            ti = tBlkStart + self.dt*i
            lockVals.append(lockVal)
            ncoVals.append(nco)
            intErrors.append(self.sumError)
            lockValsSmooth.append(self.lockVal)
            freqVals.append(self.freqEst)
            tvec.append(ti)


        # ------------DEBUGGING PLOTS-----------
        plt.figure(1,figsize=(11,8))
        plt.subplot(511)
        plt.plot(tvec,np.real(np.asarray(signalDataIn)),'r',tvec,np.real(np.asarray(ncoVals)),'b')
        plt.ylabel('Sig Amp')
        plt.legend(('Input-Real','NCO-Real'),loc='lower right')
        plt.xlim((tBlkStart,ti))
        plt.grid(True)
        plt.subplot(512)
        plt.plot(tvec,np.imag(np.asarray(signalDataIn)),'r',tvec,np.imag(np.asarray(ncoVals)),'b')
        plt.ylabel('Sig Amp')
        plt.legend(('Input-Imag','NCO-Imag'),loc='lower right')
        plt.xlim((tBlkStart,ti))
        plt.grid(True)
        plt.subplot(513)
        plt.plot(tvec,np.asarray(lockVals),'r',tvec,np.asarray(lockValsSmooth),'b',tvec,self.lockThresh*np.ones(len(tvec)),'--k')
        plt.ylabel('Energy Lock\n Metric [%]')
        plt.legend(('Raw','Smoothed','Lock Threshold'),loc='lower right')
        plt.xlim((tBlkStart,ti))
        plt.grid(True)
        plt.subplot(514)
        plt.plot(tvec,np.real(np.asarray(signalDataOut)),'r',tvec,np.imag(np.asarray(signalDataOut)),'b')
        plt.ylabel('Output Signal \n@ Baseband')
        plt.legend(('In-Phase','Quadrature'),loc='lower right')
        plt.xlim((tBlkStart,ti))
        plt.xlabel('Time [s]')    
        plt.grid(True)
        plt.subplot(515)
        plt.plot(tvec,np.asarray(intErrors)/(2*np.pi*self.dt),'r',tvec,freqVals,'b')
        plt.grid(True)
        plt.ylabel('NCO Freq [Hz]')
        plt.legend(('Raw','Smoothed'),loc='lower right')
        plt.xlim((tBlkStart,ti))
        plt.xlabel('Time [s]')
        plt.show() 
        #------------------------------------------


        self.blk = self.blk + 1

        return signalDataOut
